import random
import tkinter as tk
from tkinter import messagebox
import time
from memory_profiler import memory_usage
from tkinter import ttk
import psutil

def generate_random_preferences(num_students, num_institutions):
    # Générer aléatoirement des préférences pour les étudiants
    student_prefs = {
        f'etudiant{i}': random.sample([f'institution{j}' for j in range(num_institutions)], num_institutions)
        for i in range(num_students)
    }
    # Générer aléatoirement des préférences pour les institutions
    institution_prefs = {
        f'institution{i}': random.sample([f'etudiant{j}' for j in range(num_students)], num_students)
        for i in range(num_institutions)
    }

    return student_prefs, institution_prefs

def stable_marriage(student_preferences, school_preferences):
    # Créer des copies de listes des préférences pour l'algorithme
    student_prefs_copy = {s: prefs[:] for s, prefs in student_preferences.items()}
    students = list(student_prefs_copy.keys())
    schools = list(school_preferences.keys())

    # Initialiser le statut comme 'libres' pour tous les étudiants et écoles
    student_status = {student: None for student in students}
    school_status = {school: None for school in schools}
    student_free = students[:]

    # Tant qu'il reste des étudiants libres
    while student_free:
        student = student_free[0]
        school_pref_list = student_prefs_copy[student]
        school = school_pref_list.pop(0) # Prendre la première école préférée

        # Si l'école est libre, faire la paire
        if school_status[school] is None:
            school_status[school] = student
            student_status[student] = school
            student_free.remove(student)
        else:
            # Si l'école est prise, comparer les préférences
            current_partner = school_status[school]
            if school_preferences[school].index(student) < school_preferences[school].index(current_partner):
                # Si le nouvel étudiant est plus préféré, mettre à jour la paire
                school_status[school] = student
                student_status[student] = school
                student_free.remove(student)
                # L'ancien partenaire devient libre
                if current_partner not in student_free:
                    student_free.append(current_partner)

    return student_status

def calculate_satisfaction(pairing, preferences, reverse=False):
    # Calculer le score de satisfaction
    total_score = 0
    max_score = len(preferences)

    # Calculer le score pour chaque paire basé sur le rang de préférence
    for entity, partner in pairing.items():
        if reverse:
            rank = preferences[partner].index(entity) + 1
        else:
            rank = preferences[entity].index(partner) + 1
        score = max_score - rank + 1  # Un score plus haut pour une préférence plus haute
        total_score += score

    # Calculer la satisfaction moyenne et la normaliser
    average_satisfaction = total_score / len(pairing)
    normalized_satisfaction = average_satisfaction / max_score

    return normalized_satisfaction

def run_algorithm():
    try:
        num_students = int(student_entry.get())
        num_institutions = int(institution_entry.get())

        # Vérifier si le nombre d'étudiants est égal au nombre d'institutions
        if num_students != num_institutions:
            output_text.delete('1.0', tk.END)
            output_text.insert(tk.END, "Erreur: Le nombre d'étudiants doit être égal au nombre d'institutions.\n")
            return

        process = psutil.Process()
        start_memory = process.memory_info().rss / (1024 * 1024)  # Convertir les bytes en MB
        start_time = time.time()

        # Générer les préférences et appliquer l'algorithme
        student_preferences, school_preferences = generate_random_preferences(num_students, num_institutions)
        result = stable_marriage(student_preferences, school_preferences)

        # Calculer le temps d'exécution et la mémoire utilisée
        end_time = time.time()
        end_memory = process.memory_info().rss / (1024 * 1024)
        execution_time = end_time - start_time
        memory_used = end_memory - start_memory

        # Calculer les statistiques de choix
        choice_counts = {i: 0 for i in range(1, num_students + 1)}
        for student, school in result.items():
            rank = student_preferences[student].index(school) + 1
            choice_counts[rank] = choice_counts.get(rank, 0) + 1

        # Calculer la satisfaction des étudiants et des institutions
        student_satisfaction = calculate_satisfaction(result, student_preferences)
        institution_satisfaction = calculate_satisfaction(result, school_preferences, reverse=True)

        # Afficher les résultats
        output_text.delete('1.0', tk.END)
        output_text.insert(tk.END, "Métriques de Performance:\n")
        output_text.insert(tk.END, f"Temps d'exécution: {execution_time:.2f} secondes\n")
        output_text.insert(tk.END, f"Mémoire utilisée: {memory_used:.2f} MB\n\n")
        output_text.insert(tk.END, "Résultats des Appariements:\n")
        for student, school in result.items():
            output_text.insert(tk.END, f"{student} est apparié avec {school}\n")

        output_text.insert(tk.END, "\nStatistiques des Choix:\n")
        for rank, count in choice_counts.items():
            output_text.insert(tk.END, f"Nombre d'étudiants avec choix {rank}: {count}\n")

        output_text.insert(tk.END, f"\nSatisfaction des étudiants: {student_satisfaction}\n")
        output_text.insert(tk.END, f"Satisfaction des institutions: {institution_satisfaction}\n")

    except ValueError:
        output_text.delete('1.0', tk.END)
        output_text.insert(tk.END, "Erreur: Veuillez saisir des nombres valides pour les étudiants et les institutions.\n")


# interface
app = tk.Tk()
app.title("Stable Marriage Algorithm")
app.geometry("500x400") 

style = ttk.Style()
style.configure('TButton', font=('Arial', 10), padding=10)
style.configure('TLabel', font=('Arial', 12), padding=10)
style.configure('TEntry', font=('Arial', 12), padding=10)

frame = ttk.Frame(app, padding="10 10 10 10")
frame.pack(fill=tk.BOTH, expand=True)

# Labels
ttk.Label(frame, text="Nombre d'étudiants:").grid(column=0, row=0, sticky=tk.W)
student_entry = ttk.Entry(frame, width=15)
student_entry.grid(column=1, row=0)

ttk.Label(frame, text="Nombre d'institutions:").grid(column=0, row=1, sticky=tk.W)
institution_entry = ttk.Entry(frame, width=15)
institution_entry.grid(column=1, row=1)

run_button = ttk.Button(frame, text="Execute order 66  ", command=run_algorithm)
run_button.grid(column=0, row=2, columnspan=2)

# scrollbar
scrollbar = tk.Scrollbar(frame)
scrollbar.grid(column=2, row=3, sticky='ns')

output_text = tk.Text(frame, height=15, width=50, yscrollcommand=scrollbar.set)
output_text.grid(column=0, row=3, columnspan=2, sticky='ew')
scrollbar.config(command=output_text.yview)

app.mainloop()
