
--TD4

--Exo2
--1
select * from v$version;

-- BANNER
-- --------------------------------------------------------------------------------
-- BANNER_FULL
-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------
-- BANNER_LEGACY									     CON_ID
-- -------------------------------------------------------------------------------- ----------
-- Oracle Database 19c Enterprise Edition Release 19.0.0.0.0 - Production
-- Oracle Database 19c Enterprise Edition Release 19.0.0.0.0 - Production
-- Version 19.20.0.0.0
-- Oracle Database 19c Enterprise Edition Release 19.0.0.0.0 - Production			  0

--2

select distinct server from v$session;

--dedicated chaque utilisateur  a propre processus  un processus client pour chaque serveur 
--shared processus commun

--3
select * from v$option;


--data mining ??

--Exo3
--1
select * from v$instance;


select host_name from v$instance;

-- hote prodpeda-oracle2.umontpellier.fr
select startup_time from v$instance;

-- temp demarrage 01/10/2023

SELECT host_name, startup_time 
FROM v$instance;



--Exo 3.1

--1 byte =1.0 × 10-6 megabytes

SELECT round(bytes/1000000,2) AS "Shared Pool Size Size in MB" 
FROM v$sgainfo 
WHERE name = 'Shared Pool Size';

-- Shared Pool Size Size in MB
-- ---------------------------
-- 		     822,08
SELECT round(bytes/1024/1024,2) AS "Shared Pool Size Size in MB" 
FROM v$sgainfo 
WHERE name = 'Shared Pool Size';
-- Shared Pool Size Size in MB
-- ---------------------------
-- 			784

SELECT round(bytes/1000000,2) AS "Buffer Cache Size Size in MB" 
FROM v$sgainfo 
WHERE name = 'Buffer Cache Size';

-- Buffer Cache Size Size in MB
-- ----------------------------
-- 		      385,88
SELECT round(bytes/1024/1024,2) AS "Buffer Cache Size Size in MB" 
FROM v$sgainfo 
WHERE name = 'Buffer Cache Size';
-- Buffer Cache Size Size in MB
-- ----------------------------
-- 			 368


SELECT round(bytes/1000000,2) AS "Redo Buffers Size in MB" 
FROM v$sgainfo 
WHERE name = 'Redo Buffers';

-- Redo Buffers Size in MB
-- -----------------------
-- 		    7,6

SELECT round(bytes/1024/1024,2) AS "Redo Buffers Size in MB" 
FROM v$sgainfo 
WHERE name = 'Redo Buffers';

-- Redo Buffers Size in MB
-- -----------------------
-- 		   7,25




select sum(bytes) from v$sgainfo;


--1 byte =1.0 × 10-9 gigabytes

select round(sum(bytes)/1000000000,2) AS " Total SGA Size (GO)" from v$sgainfo;

--  Total SGA Size (GO)
-- --------------------
-- 		8,88



SELECT round(bytes/1000000,2) AS "Shared Pool Size Size in MB" 
FROM v$sgastat 
WHERE name = 'shared_io_pool';


-- Shared Pool Size Size in MB
-- ---------------------------
-- 		      67,11

SELECT round(bytes/1000000,2) AS "buffer_cache Size Size in MB" 
FROM v$sgastat
WHERE name = 'buffer_cache';
-- buffer_cache Size Size in MB
-- ----------------------------
-- 		      318,77




--select name from v$sgastat where name like '%redo%';


--Exo 3.2

SELECT (SUM(pins)-SUM(reloads)/SUM(pins)) AS "Library Cache Hit Ratio" From v$librarycache;

-- Library Cache Hit Ratio
-- -----------------------
-- 	     ,998941489

--efficacité ?


-- ### **1. System Global Area (SGA):**
-- The SGA is a group of shared memory structures that contain data and control information for one Oracle database instance. The SGA is divided into several parts:

-- #### **a. Shared Pool:**
-- This is one of the most crucial parts of the SGA. The shared pool is used to store:

-- - **Library Cache:** Contains parsed SQL statements and PL/SQL code. Every time a SQL statement is executed, Oracle tries to find it in the library cache to avoid reparsing. If it finds the statement (a "hit"), execution proceeds more quickly than if the statement had to be reparsed (a "miss").

-- - **Dictionary Cache (or Row Cache):** Contains definitions of database objects. When Oracle needs information about one of its objects (e.g., a table or index), it looks in the dictionary cache before accessing the data dictionary tables on disk.

-- #### **b. Database Buffer Cache (or Buffer Cache):**
-- Stores copies of data blocks read from data files. When a request is made to read a block, it checks if the block is already in the database buffer cache. If it's there (a "hit"), it's much faster than reading from disk (a "miss").

-- #### **c. Redo Log Buffer:**
-- Captures all changes made to data, which can be used to reconstruct data as needed. Before changes are written to the disk, they are written to the redo log buffer and then to the online redo logs.

-- ### **2. Hit Ratio:**
-- The hit ratio is a measure used to determine the efficiency of a cache. It's calculated as:

-- \[ \text{Hit Ratio} = \frac{\text{Number of Hits}}{\text{Total Accesses}} \]

-- Where:
-- - A "hit" means the required piece of data was found in the cache.
-- - A "miss" means the data was not in the cache, and a more time-consuming retrieval (typically from disk) is needed.

-- In Oracle, having a high hit ratio in the shared pool (for library and dictionary caches) and database buffer cache usually indicates good performance because it means Oracle found what it needed in memory and didn't have to go to disk.

-- ### **3. Tuning Implications:**
-- - **Library Cache:** A low hit ratio here suggests that parsed SQL statements are aging out of the cache too quickly and are being reparsed frequently. This might mean the shared pool is too small or that there's inefficient SQL or PL/SQL code.

-- - **Dictionary Cache:** A low hit ratio might suggest frequent access to the data dictionary, potentially because of recursive SQL. It might be beneficial to increase the shared pool size or optimize operations that cause high dictionary access.

-- - **Buffer Cache:** A low hit ratio here suggests Oracle is frequently reading from disk rather than memory. Consider increasing the buffer cache size or optimizing queries.

-- In essence, the Oracle memory structures aim to keep as much useful information in fast-access memory as possible to minimize the more time-consuming operations of r




-- select sql_id, substr(sql_text,1,80) as req, disk_reads from v$sqlarea where parsing_schema_name ='e20150007033';
-- col parsing_schema_name for a16
-- select parsing_schema_name, sql_id, substr(sql_text,1,80) as req from v$sqlarea order by
-- first_load_time ;
-- select parsing_schema_name, sql_id, substr(sql_text,1,80) as req from v$sqlarea where
-- parsing_schema_name<>'SYS';
-- select to_char(logon_time, 'DD/MM/YYYY HH24:MI:SS') , username, program, sql_text
-- from v$session , v$sqlarea
-- where v$session.sql_address = v$sqlarea.address
-- order by username, program;
-- select r.sql_id, disk_reads, elapsed_time, username from v$sql r, v$session s where
-- s.sql_id = r.sql_id and type='USER';
-- select sql_FullText,(cpu_time/100000) "Cpu Time (s)"
-- , (elapsed_time/1000000) "Elapsed time (s)"
-- , fetches, buffer_gets, disk_reads, executions
-- FROM v$sqlarea WHERE Parsing_Schema_Name ='P00000009432'
-- AND rownum <50
-- order by 3 desc;


CREATE OR REPLACE PROCEDURE User_Activity(p_schema_name VARCHAR2) AS
BEGIN
    FOR r IN (SELECT SQL_ID, SQL_TEXT, DISK_READS, EXECUTIONS, CPU_TIME, ELAPSED_TIME
              FROM v$sqlarea 
              WHERE PARSING_SCHEMA_NAME = p_schema_name
              ORDER BY LAST_LOAD_TIME DESC)
    LOOP
        DBMS_OUTPUT.PUT_LINE('SQL ID: ' || r.SQL_ID);
        DBMS_OUTPUT.PUT_LINE('SQL Text: ' || r.SQL_TEXT);
        DBMS_OUTPUT.PUT_LINE('Disk Reads: ' || r.DISK_READS);
        DBMS_OUTPUT.PUT_LINE('Executions: ' || r.EXECUTIONS);
        DBMS_OUTPUT.PUT_LINE('CPU Time: ' || r.CPU_TIME);
        DBMS_OUTPUT.PUT_LINE('Elapsed Time: ' || r.ELAPSED_TIME);
        DBMS_OUTPUT.PUT_LINE('------------');
    END LOOP;
END;
/

--avec curseur 
--prof ?
--exec User_Activity('E20150007033');




CREATE OR REPLACE PROCEDURE Costly_Cursors AS
BEGIN
    FOR r IN (SELECT SQL_ID, SQL_TEXT, DISK_READS, EXECUTIONS, CPU_TIME, ELAPSED_TIME
              FROM v$sqlarea 
              ORDER BY DISK_READS DESC
              FETCH FIRST 10 ROWS ONLY)
    LOOP
        DBMS_OUTPUT.PUT_LINE('SQL ID: ' || r.SQL_ID);
        DBMS_OUTPUT.PUT_LINE('SQL Text: ' || r.SQL_TEXT);
        DBMS_OUTPUT.PUT_LINE('Disk Reads: ' || r.DISK_READS);
        DBMS_OUTPUT.PUT_LINE('Executions: ' || r.EXECUTIONS);
        DBMS_OUTPUT.PUT_LINE('CPU Time: ' || r.CPU_TIME);
        DBMS_OUTPUT.PUT_LINE('Elapsed Time: ' || r.ELAPSED_TIME);
        DBMS_OUTPUT.PUT_LINE('------------');
    END LOOP;
END;
/


--Exo 3.3

--exo 1 

--exo2

-- v$bh cache de buffer


-- dba_objects vue du dictionnaire informations sur tous les objets de la base de données.


-- dirty means changed/modified blocks en memoire mais pas encore sauvegarde sur le disque 
-- l'interet est de voir les details les blocks sales.'

-- select file#, block#, class#, dirty, objd, object_name, owner
-- from v$bh, dba_objects where dirty = 'Y' and objd = object_id;

--class 1???

--on enleve condition dirty pour tous voir et on filtre par class 1 pour voir que des data blocks ???
-- select file#, block#, class#, dirty, objd, object_name, owner
-- from v$bh, dba_objects where objd = object_id and class#=1;

   -- on connait les noms grace a la joiuture car on va avoir acces par le cle etranger. 
--select * from dba_objects;
--describe dba_objects;


-- pour les noms 

-- select v.objd,d.object_name,d.object_type,d.owner
-- from v$bh v, dba_objects d
-- where v.objd=d.object_id
-- order by d.object_name;




select owner, count(*) as blocks
from v$bh, dba_objects 
where objd = object_id
group by owner
order by blocks desc
fetch first 1 row only;



-- OWNER																     BLOCKS
-- -------------------------------------------------------------------------------------------------------------------------------- ----------
-- SYS																      16887



--Exo 3.5
--1
-- describe dba_tablespaces;
select tablespace_name, status,contents,block_size from dba_tablespaces;

--2
SELECT file#, status, bytes
FROM v$datafile;
--3
SELECT group#, member, status
FROM v$logfile;

--4
SELECT name 
FROM v$controlfile;

--5

SELECT tablespace_name, file_name 
FROM dba_data_files;

SELECT tablespace_name, COUNT(*) as nb_files 
FROM dba_data_files 
GROUP BY tablespace_name;


