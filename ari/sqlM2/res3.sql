--select tablespace_name from user_tables;
--select tablespace_name from dba_tables;




-- show parameter
-- show parameter db_block_size
-- select name, value from v$parameter where name like 'db_block%';



-- analyze table EMP compute statistics


-- SELECT NUM_ROWS, BLOCKS, EMPTY_BLOCKS, AVG_SPACE, CHAIN_CNT
-- FROM DBA_TAB_STATISTICS
-- WHERE TABLE_NAME = 'EMP';

--EXO2

--create table TEST(num char(3), commentaire char(97));

create or replace procedure remplissage (borneInf in number, borneSup in number) is
i number;
comme char(97);
begin
comme := 'cot_';
for i in borneInf .. borneSup
loop
comme := dbms_random.value || i ;
insert into test values (i,comme);
end loop;
commit;
exception
when others then dbms_output.put_line(SQLCODE||'  '||SQLERRM);
end;
/ 

--1
  NUM_ROWS     BLOCKS EMPTY_BLOCKS
---------- ---------- ------------
	 0	    0		 0



--2
NUM_ROWS     BLOCKS EMPTY_BLOCKS
---------- ---------- ------------
	50	    5		 3



--3
 NUM_ROWS     BLOCKS EMPTY_BLOCKS
---------- ---------- ------------
       150	    5		 3

--4 
 NUM_ROWS     BLOCKS EMPTY_BLOCKS
---------- ---------- ------------
       250	    5		 3

--5


  NUM_ROWS     BLOCKS EMPTY_BLOCKS
---------- ---------- ------------
       350	   13		 3


-- +8 blocks pour le premiere extend! apres ca change la taille d'extend
--1.2.1

--delete from test where mod(num,3)=0;


 NUM_ROWS     BLOCKS EMPTY_BLOCKS
---------- ---------- ------------
       235	   13		 3

--truncate

truncate table test;
 NUM_ROWS     BLOCKS EMPTY_BLOCKS
---------- ---------- ------------
	 0	    0		 8


select segment_type, extent_id, bytes/1024 enKo, blocks from user_extents where
segment_name = 'TEST';






---1.3

select count(*) from user_segments where segment_name='TEST';

  COUNT(*)
----------
	 1





--TEST





select segment_type from user_segments where segment_name='TEST';

--table