---TD2
--Exo1

-- delete all triggers 

create or replace procedure deleteTriggers
as
cursor curs is select   trigger_name from user_triggers;
begin
FOR t in curs
Loop
execute immediate 'DROP TRIGGER'||t.trigger_name;
end loop;  
end;
/

CREATE OR REPLACE PROCEDURE DELETE_ALL_TRIGGERS
as
cursor curseur is select trigger_name from user_triggers;
begin
FOR rec in curseur
LOOP
EXECUTE IMMEDIATE 'DROP TRIGGER'||rec.trigger_name;
END LOOP;
end;
/



--Exo2

CREATE OR REPLACE PROCEDURE EmployesDuDepartement ( ndept in integer,lesemployes out varchar)
as
cursor employes  is select nom,num,FONCTION from EMP where N_DEPT=ndept;
BEGIN
lesemployes:='';
for e in employes
loop
lesemployes := lesemployes||' '|| e.nom||' '||e.num||' '||e.FONCTION||chr(10) ;
end loop;
if lesemployes='' or lesemployes is NULL then
dbms_output.put_line('pas de  personel!!!!!!!!');
end if;
end;
/


declare
employes varchar(1000);
begin
EmployesDuDepartement(30,employes);
dbms_output.put_line(employes);
end;
/

declare
employes varchar(1000);
begin
EmployesDuDepartement(2,employes);
dbms_output.put_line(employes);
end;
/

CREATE OR REPLACE FUNCTION CoutSalarialDuDepartement(ndept in integer)
return float is
cTotal float;
begin 
select sum(salaire) into cTotal from emp where N_DEPT=ndept;
if cTotal is NULL then
dbms_output.put_line('A l aide !!!!!!!!');
end if;
return cTotal;
end;
/


select CoutSalarialDuDepartement(10) from emp;
select CoutSalarialDuDepartement(10) from dual;

select CoutSalarialDuDepartement(2) as CoutEuros from dual;


    -- select CoutSalarialDuDepartement(10) from emp;:
    --     This will run the CoutSalarialDuDepartement function for as many rows as there are in the emp table.
    --     If your emp table has 100 rows, this will execute the function 100 times and return 100 rows, all with the same value: the total salary of department 10.
    --     This is generally not a good practice and is redundant, as you're calculating the same value over and over.

    -- select CoutSalarialDuDepartement(10) from dual;:
    --     Here, you're querying from the dual table, which is a special one-row, one-column table present by default in all Oracle databases.
    --     As a result, the function gets executed only once, and you get a single row as output, which is the expected behavior for this kind of calculation.
    --     This is the correct way to call the function.


    -- exploiter une vue dans une fonction demande ce droit (directement et non pas au
--travers d'un role)
GRANT SELECT ANY DICTIONARY TO PUBLIC ;
create or replace package supervise
as
function pourcentageConnexions return number ;
procedure lesConnectes ;
procedure lesTablesDesConnectes ;
end ;
/
create or replace package body supervise
as
function pourcentageConnexions return number
as
nbre1 integer;
nbre2 integer;
begin
select count(distinct username) into nbre1 from dba_users;
select count(username) into nbre2 from v$session where type='USER';
return nbre2/nbre1 * 100;
exception when others then dbms_output.put_line(sqlcode||' '||sqlerrm);
end;
procedure lesConnectes
is
cursor c is select rpad(s.username,14) as usager, s.module as logiciel,
rpad(s.osuser,40) as osUsager, p.program,
to_char(s.logon_time,'DD-MM-YY---HH-MM-SS') as dateConnexion, s.machine as machina,
p.pid as oracleProcess, p.spid as osProcess
from v$session s, v$process p where s.paddr = p.addr and s.type = 'USER';
begin
for t in c
loop
dbms_output.put_line(t.usager||' '||t.osUsager||' Processus Syst exploitation
'||t.osProcess||' le '||rpad(t.dateConnexion,16));
end loop ;
end ;
procedure lesTablesDesConnectes
is
cursor c1 is select count(table_name) as nbTables, sum(num_rows) as nbTuples, owner from
dba_tables d, v$session v where d.owner=v.username and v.type ='USER' group by owner
order by owner;
begin
for t1 in c1
loop
dbms_output.put_line(rpad(t1.owner,14)||' pour '||t1.nbTables||' tables et
'||t1.nbTuples||' tuples ');
dbms_output.put_line('============================');
for t2 in (select table_name as tbl, owner from dba_tables where owner = t1.owner)
loop
dbms_output.put_line(rpad(t2.tbl,36)||' '||rpad(t2.owner,14));
end loop;
end loop;
end ;
end ;
/
select supervise.pourcentageConnexions from dual;
exec supervise.lesConnectes
exec supervise.lesTablesDesConnectes
