


DROP trigger rennesMille;
DROP trigger rennesMille2;
DROP trigger Ouvrable;

set serveroutput on;

create trigger rennesMille
after
insert or update of SALAIRE on EMP 
for each row
begin
if :new.SALAIRE<1000 then 
dbms_output.put_line('a l"aide');
end if;
end;
/




create or replace trigger rennesMille2
before
insert or update of SALAIRE on EMP 
for each row
declare 
nd dept.N_DEPT%TYPE;
begin
Select N_DEPT into nd from DEPT where LIEU='Rennes';
if :new.N_DEPT =nd then
if :new.SALAIRE<1000 then
raise_application_error(-20010,'non');
end if;
end if;
end;
/


create or replace trigger Ouvrable
before delete or insert or update on EMP
begin
if (to_char(sysdate,'DY')='SAT') or (to_char(sysdate,'DY')='SUN')
then
raise_application_error(-20010, 'Modification interdite le '||to_char(sysdate,'DAY') ) ;
end if ;
end ;
/

create or replace procedure JoursEtHeuresOuvrables
is
begin
if (to_char(sysdate,'DY')='SAT') or (to_char(sysdate,'DY')='SUN')
then
raise_application_error(-20010, 'Modification interdite le '||to_char(sysdate,'DAY') ) ;
end if ;
end ;
/

create or replace trigger Ouvrable2
after delete or insert or update on EMP
begin
JoursEtHeuresOuvrables();
end;
/

--exo3
--create or replace table historique(dateOperation date, nomUser varchar(15), typeOperation varchar(15));

create or replace trigger monitor
after delete or insert or update on DEPT
for each row
declare
typeOp varchar(15);
BEGIN
if inserting then
typeOp := 'Insertion';
elsif updating then
typeOp := 'Modification';
elsif deleting then
typeOp := 'Suppression';
end if ;
insert into historique values (sysdate,user,typeOp);
end;
/

--exo4

create or replace trigger cascade
after update or delete of N_DEPT on dept
for each row
begin
if deleting then
delete from emp where N_DEPT=:old.N_DEPT;
elsif updating then
update emp set N_DEPT=:new.N_DEPT where N_DEPT=:old.N_DEPT;
end if;
end;
/


--Exo Big 4


create or replace trigger spy
before create or drop on e20150007033.schema
begin
--raise_application_error(-20001,'pas de destruction le dimanche');
dbms_output.put_line(ORA_SYSEVENT || ORA_DICT_OBJ_TYPE|| '  hi hi hi hi');
end;
/


--Exo 5
drop table QuiSeConnecte;
create table QuiSeConnecte
(c_user varchar2(30),os_user varchar2(100),userIP varchar2(100),c_date date);

CREATE OR REPLACE TRIGGER logon_db
after logon ON DATABASE
declare
o_user varchar2(100);
c_userIP varchar2(100);
begin
select sys_context('USERENV','OS_USER') into o_user from dual ;
select sys_context('USERENV','IP_ADDRESS') into c_userIP from dual ;
INSERT INTO QuiSeConnecte VALUES (user, o_user,c_userIP ,sysdate);
commit;
end;
/
