Question 1 (2 points)}
-- Donner les ordres de creation de tables et de contraintes

create table COMMUNE
as select * 
from P00000009432.COMMUNE;

(j'utilise departement1 pour cause de conflit avec une base existante)
create table DEPARTEMENT1
as select * 
from P00000009432.DEPARTEMENT;

ensuite 

alter table commune add constraint COMMUNE_PK primary key (CODEINSEE);



et 

create table DEPARTEMENT1
as select * 
from P00000009432.DEPARTEMENT;

alter table DEPARTEMENT1 add constraint DEPARTEMENT1_PK primary key (NUMDEP);
alter table COMMUNE add constraint NUMDEP_FK foreign key (NUMDEP) references DEPARTEMENT1(NUMDEP);


Question 2 (1 point)
-- Donner l'ordre de l'index non unique (de nom numdep_idx) 

CREATE index numdep_idx on COMMUNE(NUMDEP);


Question 3 (6 points) requêtes + résultats obtenus et/ou explications
-- Quelle est la hauteur de la taille de l'index COMMUNE_PK de la table COMMUNE ?  

SELECT index_name, blevel+1 as height, table_name FROM USER_INDEXES where table_name='COMMUNE' AND index_name = 'COMMUNE_PK';

INDEX_NAME															     HEIGHT TABLE_NAME
-------------------------------------------------------------------------------------------------------------------------------- ---------- --------------------------------------------------------------------------------------------------------------------------------
COMMUNE_PK																  2 COMMUNE



-- Quels sont les nombres de blocs de branches et de feuilles pour l'index COMMUNE_PK  de la table COMMUNE  

select br_blks , lf_blks from INDEX_STATS;

   BR_BLKS    LF_BLKS
---------- ----------
	 1	   79


--Pour cet index, quelle est la taille de chaque tuple présent au niveau des blocs des feuilles ?

select distinct LF_ROWS, LF_ROWS_LEN, BR_ROWS, LF_ROWS_LEN/LF_ROWS from INDEX_STATS; 

   LF_ROWS LF_ROWS_LEN	  BR_ROWS LF_ROWS_LEN/LF_ROWS
---------- ----------- ---------- -------------------
     35357	565712	       78		   16




--Par comparaison, quelle est la taille moyenne de chaque tuple de la table COMMUNE et facteur de blocage de l'espace qui tient compte de l'espace libre, et donc de la valeur de PCT_FREE de la vue USER_TABLES ? 
analyze table COMMUNE compute statistics;



SELECT AVG_ROW_LEN FROM USER_TABLES WHERE TABLE_NAME='COMMUNE';

AVG_ROW_LEN
-----------
	 53

SELECT VALUE AS DB_BLOCK_SIZE FROM V$PARAMETER WHERE NAME='db_block_size';

DB_BLOCK_SIZE
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
8192


SELECT PCT_FREE
FROM USER_TABLES 
WHERE TABLE_NAME = 'COMMUNE';


 PCT_FREE
----------
	10

On calcule le facteur de blocage donc on divise la taille des blocs par la taille des tuples:
 7373/51 ~= 144 (arrondi à l'inférieur)


On obtient le facteur de blocage (8192-8192*0.1)/53 = 139


-- Quelle est la hauteur de la taille de l'index NUMDEP_IDX de la table COMMUNE ? 

 analyze index numdep_idx validate structure;


select height from INDEX_STATS;

    HEIGHT
----------
	 2


-- Expliquer ce que renvoient les valeurs des attributs DISTINCT_KEYS et MOST_REPEATED_KEY de la vue INDEX_STATS pour l'index NUMDEP_IDX

DISTINCT KEYS = nombre de valeurs d’entrée (clés) distinctes dans l’arbre
MOST REPEATED KEY = nombre de répétitions de valeurs d’entrée (clés) dans l’arbre


Question 4 (5 points) - code PL/SQL et exemple d'utilisation et commentaires

Que renvoie la requête suivante ?
select rowid, rownum, codeinsee from commune;

elle renvoie via rowid l'adresse physique de la ligne de la table
             via rownum un numéro de ligne auto-incrémenté commençant à 1
             via codeinsee la valeur du code insee de la table commune  

MEMEBLOCQUE

CREATE OR REPLACE PROCEDURE MEMEBLOCQUE(p_codeinsee VARCHAR2) 
IS
    v_rowidCompare ROWID;
    v_blockCompare NUMBER;
BEGIN
    SELECT ROWID INTO v_rowidCompare FROM COMMUNE WHERE CODEINSEE = p_codeinsee;
    v_blockCompare := DBMS_ROWID.ROWID_BLOCK_NUMBER(v_rowidCompare);

    FOR rec IN (SELECT CODEINSEE, NOMCOMMIN FROM COMMUNE WHERE DBMS_ROWID.ROWID_BLOCK_NUMBER(ROWID) = v_blockCompare) LOOP
        DBMS_OUTPUT.PUT_LINE('Code Insee: ' || rec.CODEINSEE || ', Name: ' || rec.NOMCOMMIN);
    END LOOP;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('No record found for CODEINSEE ' || p_codeinsee);
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('An error occurred: ' || SQLERRM);
END MEMEBLOCQUE;
/

exec MEMEBLOCQUE(34172)

partie de résultat 
    Code Insee: 32444, Name: Thoux
    Code Insee: 32445, Name: Tieste-Uragnoux
    Code Insee: 32446, Name: Tillac
    Code Insee: 32447, Name: Tirent-Pont??jac
    Code Insee: 32448, Name: Touget
    Code Insee: 32449, Name: Toujouse
    Code Insee: 32450, Name: Tourdun
    Code Insee: 32451, Name: Tournan
    Code Insee: 32452, Name: Tournecoupe


NBRETUPLESPARBLOC

CREATE OR REPLACE PROCEDURE NBRETUPLESPARBLOC 
IS
  CURSOR block_cursor 
  IS
    SELECT DBMS_ROWID.ROWID_BLOCK_NUMBER(rowid) AS block_number, COUNT(*) AS row_count
    FROM COMMUNE
    GROUP BY DBMS_ROWID.ROWID_BLOCK_NUMBER(rowid);
  
  v_block_number NUMBER;
  v_row_count NUMBER;

BEGIN
  OPEN block_cursor;
  
  LOOP
    FETCH block_cursor INTO v_block_number, v_row_count;
    EXIT WHEN block_cursor%NOTFOUND;
    DBMS_OUTPUT.PUT_LINE('Block Number: ' || v_block_number || ', Number of Rows: ' || v_row_count);
  END LOOP;
  
  CLOSE block_cursor;

EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('Error: ' || SQLERRM);
    CLOSE block_cursor;
END;
/
résultat partiel
exec(NBRETUPLESPARBLOC)
    Block Number: 460050, Number of Rows: 133
    Block Number: 460051, Number of Rows: 129
    Block Number: 459737, Number of Rows: 132
    Block Number: 459746, Number of Rows: 141
    Block Number: 460038, Number of Rows: 130
    Block Number: 459738, Number of Rows: 136
    Block Number: 459764, Number of Rows: 133
    Block Number: 459713, Number of Rows: 119
    Block Number: 459726, Number of Rows: 126

On avait obtenu comme facteur de blocage 139 ce qui correspond aux valeurs observées.


Cette procédure PL/SQL utilise un curseur pour extraire les numéros de bloc pour chaque enregistrement de la table COMMUNE. Ensuite, elle parcourt ces numéros de bloc, compte le nombre de tuples dans chaque bloc en utilisant dbms_rowid.rowid_block_number, et affiche le résultat à l'aide de DBMS_OUTPUT.PUT_LINE.

Assurez-vous que le package DBMS_OUTPUT est activé dans votre session pour voir les résultats. Vous pouvez appeler la procédure de la manière suivante :

sql

BEGIN
    NBRETUPLESPARBLOC;
END;
/

Cela affichera le numéro de chaque bloc et le nombre de tuples qu'il contient dans la table COMMUNE.


Question 3 (6 points) - code PL/SQL et exemple d'utilisation

BLOCSDUDEPARTEMENT

CREATE OR REPLACE PROCEDURE BLOCSDUDEPARTEMENT(p_numdep VARCHAR2) AS
BEGIN
  FOR rec IN (SELECT DISTINCT DBMS_ROWID.ROWID_BLOCK_NUMBER(rowid) AS block_number
    FROM COMMUNE
    WHERE NUMDEP = p_numdep) LOOP
    DBMS_OUTPUT.PUT_LINE('Block Number: ' || rec.block_number);
  END LOOP;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    DBMS_OUTPUT.PUT_LINE('Departement NON Existant' || p_numdep);
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('PROBLEM: ' || SQLERRM);
END;
/



exec blocsdudepartement(34)
Block Number: 37341
Block Number: 37321
Block Number: 37340
Block Number: 459690
Block Number: 37322
Block Number: 459687

