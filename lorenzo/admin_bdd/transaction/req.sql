select
username, osuser, sid, terminal,
to_char(logon_time, 'YYYY-MM-DD HH24:MI') as logon_time
from v$session where type='USER';