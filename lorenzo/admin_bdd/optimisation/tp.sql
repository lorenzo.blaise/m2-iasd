--exo 1

-- query 1
select nomcommaj, codeinsee from commune, departement
where codeinsee = cheflieu
minus
select nomcommaj, codeinsee from commune, region
where codeinsee = cheflieu
Fetch first 2 rows only ;
-- query 2
select nomcommaj, codeinsee from commune where
codeinsee in (select cheflieu from departement)
and codeinsee not in (select cheflieu from region)
Fetch first 2 rows only ;
-- query 3
select nomcommaj, codeinsee from commune where
exists (select null from departement where codeinsee=cheflieu)
and not exists (select null from region where codeinsee=cheflieu)
Fetch first 2 rows only ;
-- query 4
select nomcommaj, codeinsee from commune, departement
where codeinsee = cheflieu
and codeinsee not in (select cheflieu from region)
Fetch first 2 rows only ;
--query 5
select nomcommaj, codeinsee from commune left join departement on codeinsee = cheflieu
where cheflieu is not null
and codeinsee not in (select cheflieu from region)
Fetch first 2 rows only ;
--query 6
select nomcommaj, codeinsee from commune join departement on codeinsee = cheflieu
minus
select nomcommaj, codeinsee from commune join region on codeinsee = cheflieu
Fetch first 2 rows only ;
--query 7
select nomcommaj, codeinsee from commune left join departement on codeinsee = cheflieu
where decode(cheflieu,null,'non','oui') = 'oui'
and codeinsee not in (select cheflieu from region)
Fetch first 2 rows only ;
--query 8
select nomcommaj, codeinsee from commune, (select cheflieu from departement) d
where codeinsee = d.cheflieu
minus
select nomcommaj, codeinsee from commune, (select cheflieu from region) r
where codeinsee = r.cheflieu
Fetch first 2 rows only ;