import random

def stable_marriage(student_preferences, school_preferences):
    n = len(student_preferences)
    # Initialize all students and schools as free
    student_status = {student: None for student in student_preferences.keys()}
    school_status = {school: None for school in school_preferences.keys()}
    
    # While there is a free student
    while None in student_status.values():
        for student in student_preferences:
            if student_status[student] is None:
                # Get the first school from the student's preference list
                school = student_preferences[student].pop(0)
                if school_status[school] is None:
                    # If the school is free, assign the pair
                    student_status[student] = school
                    school_status[school] = student
                else:
                    # If the school is not free, check preferences
                    current_partner = school_status[school]
                    if school_preferences[school].index(student) < school_preferences[school].index(current_partner):
                        # If the student is preferred, update the pairs
                        student_status[student] = school
                        student_status[current_partner] = None
                        school_status[school] = student
    
    return student_status

def generate_random_preferences(num_students, num_institutions):
    student_prefs = {student: random.sample(range(num_institutions), num_institutions) for student in range(num_students)}
    institution_prefs = {institution: random.sample(range(num_students), num_students) for institution in range(num_institutions)}

    return student_prefs, institution_prefs

def print_preferences(preferences, title):
    print(title)
    for key, value in preferences.items():
        print(f"{key}: {value}")
    print()


student_preferences, school_preferences = generate_random_preferences(10,10)

print_preferences(student_preferences,"pref students")
print_preferences(school_preferences,"pref schools")

result = stable_marriage(student_preferences, school_preferences)
print("Stable Pairings:")
for student, school in result.items():
    print(f"{student} is paired with {school}")